<!-- Modaali, joka on sisällytetty admin_viewiin -->
<div class="modal" tabindex="-1" role="dialog" id="removeimage">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Remove image</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p><?= $file ?></p>
                        <p>Are you sure you want to remove this image?</p>
                        <div class="row">
                            <div class="col-12 mb-2">
                                <a href="/admin/removeImage/<?= pathinfo($file, PATHINFO_FILENAME); ?>/<?= pathinfo($file, PATHINFO_EXTENSION); ?>" class="btn btn-md btn-light border border-dark">Yes</a>
                                <button type="button" data-dismiss="modal" class="btn btn-md btn-light border border-dark">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>