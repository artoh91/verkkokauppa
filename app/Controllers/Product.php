<?php namespace App\Controllers;

use App\Models\ProductModel;
use App\Models\ReviewModel;


class Product extends BaseController{

    public function __construct(){

        $session =\Config\Services::session();
        $session->start();
    
        $this->model = new ProductModel();
		$this->data['products'] = $this->model->getAllProducts();
		$this->data['categories'] = $this->model->getAllCategories();
    } 
    
    public function index($productid){

        $data = $this->data;
        $data['product'] = $this->model->find($productid);

        $model = new ReviewModel();
        $data['reviews'] = $model->getAllReviewsForThisProduct($productid);

        echo view ('templates/header', $data);
        echo view ('product_view', $data);
        echo view ('templates/footer');
        
    }

    // Ongelma: Ei käytetä return redirectia, mikä ei muuta URL:ia. Eli jos refreshaa, funktio toistuu vaikkei haluaisi.
    public function sendReview($id) {
        $data = $this->data;

        $inputData = [
            'rating' => $this->request->getVar('rating'),
            'writer' => $this->request->getVar('name'),
            'comment' => $this->request->getVar('comment'),
            'product_ID' => $id,
        ];

        $model = new ReviewModel();
        $model->ReviewToDatabase($inputData);

        $this->index($id);
    }
}