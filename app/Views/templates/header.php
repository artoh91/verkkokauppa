<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/frontpage_style.css">
    <link rel="stylesheet" href="/css/modal_style.css">
    <link rel="stylesheet" href="/css/admin_style.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" 
              rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" 
              crossorigin="anonymous">
    <link rel="stylesheet" href="/css/cart.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <title>SuperStore.com - Where price & quality meet-</title>
</head>
<?php include "../app/Views/login/login_modal.php" ?>
<?php include "../app/Views/login/register_modal.php" ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top justify-content-between"> <a class="navbar-brand"
        href="#">
        <a href="/"><img class="rounded" src="/img/logopng.png" style="max-width: 210px;" alt="logo"></a>
    </a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02"
        aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation"> <span
            class="navbar-toggler-icon"></span> </button>
    <div class="text-right collapse navbar-collapse col-9" id="navbarColor02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item mr-4">
                <div class="btn-group dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Product categories
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div>
                            <a href="/" class="dropdown-item">Show All</a>
                            <?php foreach($categories as $key => $category): ?>
                            <a href="/home/selectCategory/<?= $category->ID; ?>"
                                class="dropdown-item"><?= $category->name; ?></a>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item mr-4">
                <form action="/home/search/<?php if (isset($selectedCategory)){ echo $selectedCategory[0]->ID; } else { echo 0;} ?>" class="form-inline">
                    <div class="form-group">
                        <input class="searchbar form-control mr-sm-2" type="text" name="search" placeholder="Search products...">
                    </div>
                    <button class="btn btn-secondary my-2 my-sm-0" type="submit"><i class="marginr fa fa-search"></i>
                        Search</button>
                </form>
            </li>
            <li class="nav-item active">
                <!-- Näytä login/logout riippuen siitä, onko kirjauduttu -->
                <?php 
                  		if (!isset($_SESSION['user'])) {
                        echo '<a class="nav-link active loginbutton" href="#" data-abc="true"><i class="marginr fa fa-sign-in"></i> Login</a>';
                      }
                      else {
                        echo '<a class="nav-link active" href="/login/logout" data-abc="true"><i class="marginr fa fa-sign-out"></i> Logout</a>';
                      }
                ?>
            </li>
            <li class="nav-item active"><a class="nav-link active" href="/cart_view" data-abc="true"><i
                        class="marginr fa fa-shopping-cart"></i> Cart <?= count($_SESSION['cart']);?></a></li>
            <!-- Näytä Admin-painike, jos admin on kirjautunut -->
            <?php
                      if (isset($_SESSION['user']) && $_SESSION['user']->username === 'admin123') {
                        echo '<li class="marginl nav-item active"><a class="nav-link active" href="/admin" data-abc="true"><i class="marginr fa fa-unlock"></i> Admin</a></li>';
                      }
                ?>
            </li>
        </ul>
    </div>
</nav>
<body>