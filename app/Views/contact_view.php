<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact info</title>
    <link href="<?php echo base_url("assets/bootstrap/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CodeIgniter Contact Form Example</title> 
</head>
<body class="text-center">
    <form method="post" action="/Contact/savedata/" class=> 
<div style="margin-top: 100px;" class="col-md-12 col-md-offset-3 text-center">
    <div class="text-center">
        <div class="panel-heading">
            <h3 class="text-dark">Contact Our Support</h3>
        </div>
        <section id="cover" class="min-vh-100">
    <div id="cover-caption">
        <div class="container">
            <div class="row text-white">
                <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4">
                    <div class="px-2">
                        <form action="/Contact/savedata/" class="justify-content-center">
                            <div class="form-group">
                                <label class="sr-only">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Your name" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Email</label>
                                <input type="text" class="form-control" name="mail" placeholder="Email" required>
                            </div>
			                <div class="form-group">
                                <label class="sr-only">Message</label>
                                <textarea class="form-control" name="message" rows="4" placeholder="Message" maxlength="10000" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-danger btn-lg">Send</button> 
                        </form>
                        
                    </div>
                </div>
            </div><img src="/img/dog.jpg" style="width: 80%; border-style: solid; border-radius: 5px;">
        </div>
    </div>
</section>
</body>
</html>