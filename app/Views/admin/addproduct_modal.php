<!-- Modaali, joka on sisällytetty admin_viewiin -->
<div class="modal" tabindex="-1" role="dialog" id="addproduct">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Add new product</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p>Fill in the fields and press submit.</p>
                        <p>
                            <span class="message">
                                <?php
                                    if (isset($message)) {
                                        echo $message;
                                    }
                                ?>
                            </span>
                        </p>
                        <?php
                        // Ei käytössä, nyt on manuaalisesti toteutettu virheilmoitus
                        // echo \Config\Services::validation()->listErrors();
                        ?>
                        <form action="/admin/addProduct" class="bg-info border border-dark p-2" method="post">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Product name:</label>
                                        <input class="form-control border border-dark" name="name" type="text"
                                            placeholder="Enter product name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Description:</label>
                                        <textarea class="form-control border border-dark" name="description" placeholder="Enter description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Picture:</label>
                                        <select class="form-control border border-dark" name="picture" type="dropdown"
                                            required>
                                            <?php foreach($_SESSION['images'] as $image): ?>
                                                <?php echo "<option value=" . $image . ">" . $image . "</option>"; ?>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Price (€):</label>
                                        <input class="form-control border border-dark" name="price" type="text"
                                            placeholder="Enter price" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Sale (%):</label>
                                        <input class="form-control border border-dark" name="sale" type="text"
                                        placeholder="Enter sale percentage">
                                    </div>
                                    <div class="form-group">
                                        <label>Storage amount:</label>
                                        <input class="form-control border border-dark" name="storageamount" type="text"
                                            placeholder="Enter storage amount" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Category:</label>
                                        <select class="form-control border border-dark" name="category_ID" type="dropdown"
                                            required>
                                            <?php foreach($categories as $category): ?>
                                                <?php echo "<option value=". $category->ID .">" . $category->name . "</option>"; ?>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-2">
                                    <button type="submit"
                                        class="btn btn-md btn-light border border-dark">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>