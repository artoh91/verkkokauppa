<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model {

    public function loginCheck($username, $password) {       
        $db = db_connect();
        $builder = $db->table('customer');
        $builder->where('username', $username);
        if ($builder) {
            $query = $builder->get();
            $row = $query->getRow();
        }
        if ($row) {
            if (password_verify($password, $row->password)) {
                return $row;
            }
        }
        return null;
    }

    public function registerRun($inputData) {
        $db = db_connect();
        $builder = $db->table('customer');
        $builder->insert($inputData);
    }

    public function getAllUsers() {
        $db = db_connect();
        $builder = $db->table('customer');    
        $builder->orderBy('ID');
        $query = $builder->get();
        return $query->getResult();
    }
}
