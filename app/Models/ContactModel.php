<?php namespace App\Models;

use CodeIgniter\Model;

class ContactModel extends Model {
 	
     protected $table = 'contact';
     protected $allowedFields = ['name','mail','message'];

 public function savecontact($inputData){
     

     $db = db_connect();
     $builder = $db->table('contact');
     
     $data = [          
            'name' => $inputData["name"],
            'mail' => $inputData["mail"],
            'message' => $inputData["message"]
     ];

	 $builder->insert($data);
	
}
	 
public function getAllContacts() {
	$db = db_connect();
	$builder = $db->table('contact');    
	$builder->orderBy('saved', 'DESC');
	$query = $builder->get();
	return $query->getResult();
}

public function markAsRead($value) {
	$db = db_connect();
	$builder = $db->table('contact');
	if ($value === 'all') {
		$builder->get();
	}
	else {
		$builder->where('ID', $value);
	}
	
	$data = [
		'isread' => true,
	];

	$builder->update($data);
}

public function remove($value) {
	$db = db_connect();
	$builder = $db->table('contact');
	if ($value === 'all') {
		$builder->where('isread', true);
	}
	else {
		$builder->where('ID', $value);
	}
	$builder->delete();
}

}