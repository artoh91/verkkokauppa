<!-- Footer -->
 

<footer class="bg-dark page-footer font-small dark pt-4">
    <div class="container-fluid text-center text-md-left">
      <div class="row">
        <div class="col-md-5 mt-md-0 mt-3">

          <h5 class="marginl text-uppercase text-white">Contact us</h5>
          <p class="marginl text-white"><i class="fa fa-phone"></i> 010 309 5555</p>
          <p class="marginl text-white">Mon-Fri 9-20<br>Sat 9-17<br>Sun closed</p>
          <p class="marginl text-white"><i class="fa fa-envelope"></i> customerservice@superstore.com</p>
          <a class="text-white" href="/contact"><br>Send a message</a>
        </div>
        <hr class="clearfix w-100 d-md-none pb-3">
        <div class="col-md-4 mb-md-0 mb-3">
          <h5 class="text-uppercase text-white">Social media</h5>
          <ul class="list-unstyled">
            <li>
              <i class="fa fa-facebook-square text-white"></i> <a class="logomargin text-white" href="https://facebook.com">Facebook</a>
            </li>
            <li>
                <i class="fa fa-instagram text-white"></i> <a class="logomargin text-white" href="https://instagram.com">Instagram</a>
            </li>
            <li>
                <i class="fa fa-twitter text-white"></i> <a class="logomargin text-white" href="https://twitter.com">Twitter</a>
            </li>
            <li>
                <i class="fa fa-youtube-play text-white"></i> <a class="logomargin text-white" href="https://youtube.com">Youtube</a>
            </li>
            <li>
                <i class="fa fa-vk text-white"></i> <a class="logomargin text-white" href="https://vk.com">VK.com</a>
            </li>
          </ul>
        </div>
        <div class="col-md-3 mb-md-0 mb-3">
          <h5 class="text-uppercase text-white">Store opening hours</h5>
          <ul class="list-unstyled">
            <li>
                <a class="text-white">Mon-Fri 9-21</a>
            </li>
            <li>
                <a class="text-white">Sat 9-19</a>
            </li>
            <li>
                <a class="text-white">Sun 11-19</a>
            </li>
            <li>
                <a class="text-white" href="terms#paymentoptions"><br>Payment options</a>
                <a class="text-white" href="terms"><br>Store locations & opening hours</a>
            </li> 
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright text-center py-3 text-white">© 2020 Copyright:
      <a>OAMK TIK19SP TEAM 4</a>
    </div>
  </footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="/js/login_code.js" type="text/javascript"></script>
<script src="/js/admin_code.js" type="text/javascript"></script>
<script src="/js/frontpage_code.js" type="text/javascript"></script>
</body>
</html>