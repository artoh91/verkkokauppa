$(document).ready(function () {

    // Kun painetaan login buttonia, näytä login modal (ja estä # merkin lisäys osoitteeseen)
    $(".loginbutton").click(function () {
        event.preventDefault();
        $("#login").modal({backdrop: 'static', keyboard: true});
    });

    // Kun painetaan register buttonia, näytä register modal (ja estä # merkin lisäys osoitteeseen)
    $(".registerbutton").click(function () {
        event.preventDefault();
        $("#login").modal('hide');
        $("#register").modal({backdrop: 'static', keyboard: true});
    });

    // Kun painetaan cancel buttonia, näytä login modal
    $(".cancelbutton").click(function () {
        $("#register").modal('hide');
        $("#login").modal({backdrop: 'static', keyboard: true});
    });
    
    // Kun painetaan näitä nappeja, poista mahdollinen ilmoitusviesti modaaleista
    $(".loginbutton, .registerbutton, .cancelbutton").click(function () {
        $(".message").html('');
        $(".message").removeClass('bg-danger');
        $(".message").removeClass('bg-success');
    });
    
});