$(document).ready(function () {

    // Kun pidetään hiirtä kortin yllä
    $(".card").mouseenter(function () {
        $(this).children(".topleft").show();
    });

    // Kun ei enää pidetä hiirtä kortin yllä
    $(".card").mouseleave(function () {
        $(this).children(".topleft").hide();
    });

    // Popoverin koodi
    $(".paskaa").popover({
        trigger: "hover"
    });
    
    $(".epilepsia").click(function () {
        event.preventDefault();
        if (window.confirm("EPILEPSY WARNING! Proceed?") === true) {
            $(".hidden").show();
        }
    });

    $(".yllari").click(function () {
        if (window.confirm("HEART ATTACK WARNING! Proceed?") === true) {
            var audioElement = document.createElement('audio');
            audioElement.setAttribute('src', '/img/salaisuuksia/gragas.mp3');
            audioElement.volume = 0.75;
            audioElement.play();
            var audioElement = document.createElement('audio');
            audioElement.setAttribute('src', '/img/salaisuuksia/suspense.mp3');
            audioElement.volume = 0.75;
            audioElement.play();
            $(".gragas").show();
            $(".gragashand").show();
            setTimeout(
                function() 
                {
                    $(".gragas").hide();
                    $(".gragashand").hide();
                }, 5000
            );
        }
    });

    $(".tahdet").hover(
        function(){
            $(this).addClass("checked");
            $(this).prevAll().addClass("checked");
            $(this).nextAll().removeClass("checked");
    },
        function(){
            $(this).removeClass("checked");
            $(this).prevAll().removeClass("checked");
        }
    );

    $('.tahdet').click(function() {
            $('.tahdet').removeClass('selected');
            $(this).addClass('selected');
            $(this).prevAll().addClass('selected');
            let rating = $(this).val();
            $("[name=rating]").val(rating);
     });
});