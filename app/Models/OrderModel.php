<?php namespace App\Models;
use CodeIgniter\Model;
use App\Models\CustomerModel;
use App\Models\OrderlineModel;

class OrderModel extends Model{
    protected $table='Orderlist';

    protected $allowedFields =['status','customer_ID'];

    public function archive($customer,$cart){

        //aloita transaktio
        $this->db->transStart();
        
        
        //tallennetaan asiakkaan tiedot
        $customer_ID=$this->saveCustomer($customer);
      
        //tallennetaan tilauksen tiedot
        $order_ID=$this->saveOrder($customer_ID);
        
        //print "order saved,id=$order_id";
        
        //tallennetaan tilausrivit
        $this->saveOrderline($order_ID,$cart);

        //commit transaktio
        $this->db->transComplete();

        if($this->db->transStatus()===FALSE){

            //rollback
            $this->db->transRollback();

            return false;
        }
        else 
            return true;

    }

    private function saveCustomer($customer){
       $customerModel=new customerModel();
        $customerModel->save($customer);
      

        return $this->insertID();

    }

    private function saveOrder($customer_ID){
        $this->save([
            'customer_ID'=>$customer_ID,
            'status'=>'ordered'

        ]);
            return $this->insertID();
    }
    private function saveOrderline($order_ID,$cart){
       $orderlineModel = new OrderlineModel;
       $counts = array_count_values($cart);

       foreach(array_unique($cart) as $product_ID){

       $orderlineModel->save([
        'amount'=>$counts[$product_ID],
        'product_ID'=>$product_ID,
        'order_ID'=>$order_ID,

        


       ]);
    
    }

    }
}