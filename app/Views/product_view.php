<div class="container-fluid">
    <div class="row">
        <!--tuotteen nimi ja hinta -->
        <div class="col-4 text-center mt-2 product">
            <h2><?=$product['name']; ?></h2>
            <figure>
                <img class="img-fluid" style="max-width: 18rem;" src="/img/<?= $product['picture'] ?>" alt="">
                <figcaption>
                    <h4>Price: <?= number_format($product['price'] * ((100 - $product['sale'])/100), 2); ?> €</h4>
                </figcaption>
            </figure>
        </div>
        <!--Tuotteen tietoja ja varasto määrä -->
        <div class="col-8 mt-5">
            <div class="productview">
                <div>
                    <p>Product description:</p>
                    <p><?=$product['description']; ?></p>
                </div>

                <div class="mt-5">
                    <p>Amount in storage: <?=$product['storageamount']; ?></p>
                    <form method="post" action="<?=base_url('Cart/Add/'.$product['ID']) ?>">
                        <button <?php if ($product['storageamount'] < 1) {echo "disabled"; } ?>
                            class="btn btn-secondary mt-auto"><i class="fa fa-shopping-basket fa-md"></i> Add to
                            cart</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <div class="row">
                <div class="col-5 mb-3">
                    <h3 class="mb-3"><i class="fa fa-pencil-square fa-md"></i> Write review of this product</h3>
                    <form action="/product/sendReview/<?=$product['ID']; ?>" method="post" class="rating">
                        <label>Rating: </label>
                        <div class="form-group">
                            <input type="hidden" name="rating" value="0">
                            <button type="button" class="fa fa-star fa-lg tahdet" value="1"></button>
                            <button type="button" class="fa fa-star fa-lg tahdet" value="2"></button>
                            <button type="button" class="fa fa-star fa-lg tahdet" value="3"></button>
                            <button type="button" class="fa fa-star fa-lg tahdet" value="4"></button>
                            <button type="button" class="fa fa-star fa-lg tahdet" value="5"></button>
                        </div>
                        <div class="form-group">
                            <label>Your name:</label>
                            <input class="form-control border border-dark" name="name" type="text" required>
                        </div>
                        <div class="form-group">
                            <label>Comment:</label>
                            <textarea class="form-control border border-dark" name="comment" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-md btn-light border border-dark">Send</button>
                    </form>
                </div>
                <div class="col-7">
                    <h3 class="mb-3"><i class="fa fa-comments fa-md"></i> Reviews (<?php echo count($reviews); ?>)
                    </h3>
                    <?php foreach($reviews as $review): ?>
                    <div class="review mb-3">
                        <h5>By <span><?= $review->writer ?></span></h5>
                        <?php 
                        for ($i = 0; $i < $review->rating; $i++) {
                            echo '<i class="fa fa-star fa-md pb-2"></i>';
                        }
                        ?>
                        <p class="m-0 p-0"><?= $review->comment ?></p>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>