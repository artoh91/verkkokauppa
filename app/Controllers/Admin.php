<?php namespace App\Controllers;

use App\Models\ProductModel;
use App\Models\UserModel;
use App\Models\ContactModel;
use App\Models\ReviewModel;

// Handlaa adminin muokkausmahdollisuudet
// Tätä sivua voisi siivota tiiviimmäksi (koodia toistuu)
// Ongelma: Ei käytetä return redirectia, mikä ei muuta URL:ia. Eli jos refreshaa, funktio toistuu vaikkei haluaisi.
class Admin extends BaseController {
    
	public function __construct() {
		$session = \Config\Services::session();
        $session->start();

        $this->model = new UserModel();
        $this->data['users'] = $this->model->getAllUsers();

        $this->model = new ContactModel();
        $this->data['contacts'] = $this->model->getAllContacts();

        $this->model = new ReviewModel();
        $this->data['reviews'] = $this->model->getAllReviews();
        
        $this->model = new ProductModel();
        // Admin näkee tuotteet, jotka on piilotettu asiakkailta
		$this->data['products'] = $this->model->getAllProductsDespiteVisibility();
        $this->data['categories'] = $this->model->getAllCategories();

        $folder = "../public/img";
        $handle = opendir($folder);
        if ($handle) {
            $_SESSION['images'] = array();
            while (false !== ($file = readdir($handle))) {
            $file_ending = pathinfo($file, PATHINFO_EXTENSION);
            if (strtoupper($file_ending) === 'PNG' || strtoupper($file_ending) === 'JPG' || strtoupper($file_ending) === 'JPEG' || strtoupper($file_ending) === 'GIF') {
                $path = $folder . '/' . $file;
                array_push($_SESSION['images'], $file);
                }
            }
        }
    }
	
	public function index() {
        $data = $this->data;

        // Kirjautuneen käyttäjän tulee olla admin, jotta sivu näyttäytyy
        if (!isset($_SESSION['user']) || !$_SESSION['user']->username === 'admin123') {
            return redirect('/');
        }

        if (isset($this->message)) {
            $data['message'] = $this->message;
        } 
        if (isset($this->id)) {
            $data['id'] = $this->id;
        } 
        else {
            $data['id'] = null;
        }
        if (isset($this->table)) {
            $data['table'] = $this->table;
        } 
        else {
            $data['table'] = null;
        }
        if (isset($this->item1)) {
            $data['item1'] = $this->item1;
        }
        if (isset($this->item2)) {
            $data['item2'] = $this->item2;
        }
        if (isset($this->file)) {
            $data['file'] = $this->file;
        }
        if (isset($this->selection)) {
            $selection = $this->selection;
        }
        else {
            $selection = 'Products';
        }
        
		echo view('templates/header', $data);
		echo view('admin/admin_view', $data);
        echo view('templates/footer');
        
        echo
        '<script type="text/JavaScript">
            $("#' . $selection . '").show();
        </script>';
    }

    public function addImage() {
        $this->selection = 'Images';
        if (!$this->validate([
            'picture' => [
                'uploaded[picture]',
                'mime_in[picture,image/png,image/jpg,image/jpeg,image/gif]',
                'is_image[picture]',
                'max_size[picture,4096]',
            ]
        ])) {
            $this->message = 'Something went wrong. File type may be invalid.';
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-danger");
                $("#addimage").modal({backdrop: "static", keyboard: true});
            </script>';
        }
        else {
            $image = $this->request->getFile('picture');
            $path = APPPATH;
            $path = str_replace('app\\', 'public/img/' , $path);
            $image->move($path);

            $this->message = 'Success!';
            $this->__construct();
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-success");
                $("#addimage").modal({backdrop: "static", keyboard: true});
            </script>';
        }
    }

    public function removeImageButton($name, $type) {
        $this->selection = 'Images';
        $this->file = $name . '.' . $type;
        $this->index();
        echo
        '<script type="text/JavaScript">
            $("#removeimage").modal({backdrop: "static", keyboard: true});
        </script>';
    }

    public function removeImage($name, $type) {   
        $this->selection = 'Images'; 
        $path = APPPATH;
        $path = str_replace('app\\', 'public/img/' , $path);  
        $image = $path . $name . '.' . $type;

        if (file_exists($image)) {
            unlink($image);
        }
        $this->__construct();
        $this->index();
    }

    public function addProduct() {
        $this->selection = 'Products';
        $model = $this->model;

        $checklist = [];
        $categoryID = $this->request->getVar('category_ID');
        $categories = $model->getCategoryIDs();
        foreach ($categories as $category) {
            array_push($checklist, $category->ID);
        }

        if (!in_array($categoryID, $checklist) || !$this->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'sale' => 'numeric|permit_empty',
            'storageamount' => 'required|numeric',
            'category_ID' => 'required|integer',
        ])) {
            $this->message = 'Some fields have faulty values.';
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-danger");
                $("#addproduct").modal({backdrop: "static", keyboard: true});
            </script>';
        }
        else {
            $inputData = [
                'name' => $this->request->getVar('name'),
                'description' => $this->request->getVar('description'),
                'picture' => $this->request->getVar('picture'),
                'price' => $this->request->getVar('price'),
                'sale' => $this->request->getVar('sale'),
                'storageamount' => $this->request->getVar('storageamount'),
                'category_ID' => $this->request->getVar('category_ID'),
            ];

            $model->addProductToDatabase($inputData);

            $this->message = 'Success!';
            $this->__construct();
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-success");
                $("#addproduct").modal({backdrop: "static", keyboard: true});
            </script>';
        }
    }
    
    public function editProduct($id, $table) {
        $this->selection = 'Products';
        $model = $this->model;

        $checklist = [];
        $categoryID = $this->request->getVar('category_ID');
        $categories = $model->getCategoryIDs();
        foreach ($categories as $category) {
            array_push($checklist, $category->ID);
        }

        if (!in_array($categoryID, $checklist) || !$this->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'sale' => 'numeric|permit_empty',
            'storageamount' => 'required|numeric',
            'category_ID' => 'required|integer',
        ])) {
            $this->item1 = $model->getProductOrCategory($id, $table);
    
            $this->id = $id;
            $this->table = $table;
            
            $this->message = 'Some fields have faulty values.';
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-danger");
                $("#editproduct").modal({backdrop: "static", keyboard: true});
            </script>';
        }
        else {
            $inputData = [
                'name' => $this->request->getVar('name'),
                'description' => $this->request->getVar('description'),
                'picture' => $this->request->getVar('picture'),
                'price' => $this->request->getVar('price'),
                'sale' => $this->request->getVar('sale'),
                'storageamount' => $this->request->getVar('storageamount'),
                'category_ID' => $this->request->getVar('category_ID'),
            ];

            $model->editProductInDatabase($id, $inputData);

            $this->item1 = $model->getProductOrCategory($id, $table);
    
            $this->id = $id;
            $this->table = $table;

            $this->message = 'Success!';
            $this->__construct();
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-success");
                $("#editproduct").modal({backdrop: "static", keyboard: true});
            </script>';
        }
    }

    public function addCategory() {
        $this->selection = 'Categories';
        $model = $this->model;

        if (!$this->validate([
            'name' => 'required|min_length[1]',
        ])) {
            $this->message = 'An error occurred.';
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-danger");
                $("#addcategory").modal({backdrop: "static", keyboard: true});
            </script>';
        }
        else {
            $name = $this->request->getVar('name');

            $model->addCategoryToDatabase($name);

            $this->message = 'Success!';
            $this->__construct();
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-success");
                $("#addcategory").modal({backdrop: "static", keyboard: true});
            </script>';
        }
    }

    public function editCategory($id, $table) {
        $this->selection = 'Categories';
        $model = $this->model;

        if (!$this->validate([
            'name' => 'required|min_length[1]',
        ])) {
            $this->item2 = $model->getProductOrCategory($id, $table);
    
            $this->id = $id;
            $this->table = $table;

            $this->message = 'An error occurred.';
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-danger");
                $("#editcategory").modal({backdrop: "static", keyboard: true});
            </script>';
        }
        else {
            $name = $this->request->getVar('name');

            $model->editCategoryInDatabase($id, $name);

            $this->item2 = $model->getProductOrCategory($id, $table);
    
            $this->id = $id;
            $this->table = $table;

            $this->message = 'Success!';
            $this->__construct();
            $this->index();
            echo
            '<script type="text/JavaScript">
                $(".message").addClass("bg-success");
                $("#editcategory").modal({backdrop: "static", keyboard: true});
            </script>';
        }
    }

    public function editButtons($id, $table) {
        $model = $this->model;

        if ($table === 'product') {
            $this->item1 = $model->getProductOrCategory($id, $table);
            $this->selection = 'Products';
        }
        else {
            $this->item2 = $model->getProductOrCategory($id, $table);
            $this->selection = 'Categories';
        }

        $this->id = $id;
        $this->table = $table;
        $this->index();
        if ($table === 'product') {
            echo
            '<script type="text/JavaScript">
            $("#editproduct").modal({backdrop: "static", keyboard: true});
            </script>';
        }
        else {
            echo
            '<script type="text/JavaScript">
            $("#editcategory").modal({backdrop: "static", keyboard: true});
            </script>';
        }
    }

    public function removeButtons($id, $table) {
        $model = $this->model;

        if ($table === 'category') {
            $this->id = $id;
            $this->table = $table;
            $this->selection = 'Categories';
        }
        else {
            $this->id = $id;
            $this->table = $table;
            $this->selection = 'Products';
        }
        $this->index();
        echo
        '<script type="text/JavaScript">
        $("#remove").modal({backdrop: "static", keyboard: true});
        </script>';
    }

    public function removeProductOrCategory($id, $table) {
        $model = $this->model;

        if ($table === 'category') {
             $this->selection = 'Categories';

            $checklist = [];
            $products = $model->getProductCategoryIDs();

            foreach ($products as $product) {
                array_push($checklist, $product->category_ID);
            }

            if (in_array($id, $checklist)) {
                $this->message = "Can't delete category while it contains products.";
                $this->id = $id;
                $this->table = $table;
                $this->index();
                echo
                '<script type="text/JavaScript">
                    $(".message").addClass("bg-danger");
                    $("#remove").modal({backdrop: "static", keyboard: true});
                </script>';
            }
            else {
                $model->remove($id, $table);
        
                $this->__construct();
                $this->index();
            }
        }
        else {
            $this->selection = 'Products';
            $model->remove($id, $table);
    
            $this->__construct();
            $this->index();
        }
    }

    public function markMessage($value) {
        $model =  new ContactModel();

        $model->markAsRead($value);

        $this->selection = 'Messages';
        $this->__construct();
        $this->index();
    }

    public function removeMessage($value) {
        $model =  new ContactModel();

        $model->remove($value);

        $this->selection = 'Messages';
        $this->__construct();
        $this->index();
    }

    public function changeProductVisibility($id) {
        $model = $this->model;
        $model->changeVisibility($id);

        $this->selection = 'Products';
        $this->__construct();
        $this->index();
    }

    public function removeReview($value) {
        $model =  new ReviewModel();

        $model->remove($value);

        $this->selection = 'Reviews';
        $this->__construct();
        $this->index();
    }
}