<?php namespace App\Controllers;

use App\Models\ProductModel;

class Cart extends BaseController{

    private $ProductModel = null;

    public function __construct(){

        $session =\Config\Services::session();
        $session->start();
    
        $this->model = new ProductModel();
		$this->data['products'] = $this->model->getAllProducts();
        $this->data['categories'] = $this->model->getAllCategories();

        if(!isset($_SESSION['cart'])){
            $_SESSION['cart'] = array();
        }

    } 
    
    public function index(){
        $data = $this->data;
       
        if(count($_SESSION['cart']) > 0){
            $product = $this->model->getCartProducts($_SESSION['cart']);
        } else{
            $product = array();
        }

        $data['products'] = $product;
        
        echo view ('templates/header', $data);
        echo view ('cart_view', $data);
        echo view ('templates/footer');  
    }

//tuotteiden lisäys
    public function Add($product_id){
        
        $product=$this->request->getPost('product');
        array_push($_SESSION['cart'], $product_id);
        return redirect('/');
    }
//tuotteiden poisto
    public function empty(){
        $_SESSION['cart'] = array();
        return redirect('/');
    }
//tuotteen määrän muuttaminen (+) ostoskorissa
    public function AddProduct($product_id){
        
        $product=$this->request->getPost('product');
        array_push($_SESSION['cart'], $product_id);
        return redirect('cart_view');
    }

//tuotteen määrän muuttaminen(-) ostoskorissa
    public function RemoveProduct($product_id){
        $row = -1;

        foreach($_SESSION['cart'] as $key => $value):
            $row++;
            if($value === $product_id){
                break;
            }
        endforeach;

        array_splice($_SESSION['cart'], $row, 1);
        return redirect('cart_view');
    }
//Poista yksitäinen tuote ostoskorista
    public function Remove($product_id){
        $row = -1;
        foreach($_SESSION['cart'] as $key => $value):
            if($value === $product_id){
                $carty = array_search($value, $_SESSION['cart']);
                array_splice($_SESSION['cart'], $carty, 1);
                if(count($_SESSION['cart']) === 0){
                    $_SESSION['cart'] = array();
                }
            }
        endforeach;
        //array_splice($_SESSION['cart'], $row, 1);
        return redirect('cart_view');
    }
//order nappula
    public function order(){
        $model = $this->model;
		$data = $this->data;
        echo view ('templates/header', $data);
        echo view ('order_view', $data);
        echo view ('templates/footer');
    }
}