<!-- Modaali, joka on sisällytetty headeriin -->
<div class="modal" tabindex="-1" role="dialog" id="register">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Register</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p>Please type in your credentials and press submit when done.</p>
                        <p>
                            <span class="message">
                                <?php
                                    if (isset($message)) {
                                        echo $message;
                                    }
                                ?>
                            </span>
                        </p>
                        <?php
                        // Ei käytössä, nyt on manuaalinen virheilmoitus
                        // echo \Config\Services::validation()->listErrors();
                        ?>
                        <form action="/login/registerSubmit" class="bg-info border border-dark p-2" method="post">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>First name:</label>
                                        <input class="form-control border border-dark" name="firstname" type="text"
                                            placeholder="Enter first name" maxlength="50">
                                    </div>
                                    <div class="form-group">
                                        <label>Last name:</label>
                                        <input class="form-control border border-dark" name="lastname" type="text"
                                            placeholder="Enter last name" maxlength="50">
                                    </div>
                                    <div class="form-group">
                                        <label>Username:</label>
                                        <input class="form-control border border-dark" name="username" type="text"
                                            placeholder="Enter username" minlength="8" maxlength="50" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Password:</label>
                                        <input class="form-control border border-dark" name="password" type="password"
                                            placeholder="Enter password" minlength="8" maxlength="50" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm password:</label>
                                        <input class="form-control border border-dark" name="passwordconfirm"
                                            type="password" placeholder="Confirm password" minlength="8" maxlength="50"
                                            required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Email address:</label>
                                        <input class="form-control border border-dark" name="email" type="email"
                                            placeholder="Enter email address" maxlength="255">
                                    </div>
                                    <div class="form-group">
                                        <label>Mobile number:</label>
                                        <input class="form-control border border-dark" name="mobilenumber" type="text"
                                            placeholder="Enter mobile number" maxlength="15">
                                    </div>
                                    <div class="form-group">
                                        <label>Home address:</label>
                                        <input class="form-control border border-dark" name="address" type="text"
                                            placeholder="Enter home address" maxlength="25">
                                    </div>
                                    <div class="form-group">
                                        <label>Post code:</label>
                                        <input class="form-control border border-dark" name="postnumber" type="text"
                                            placeholder="Enter post code" maxlength="8">
                                    </div>
                                    <div class="form-group">
                                        <label>Post office:</label>
                                        <input class="form-control border border-dark" name="postoffice" type="text"
                                            placeholder="Enter post office" maxlength="25">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center mb-2">
                                    <button type="submit"
                                        class="btn btn-md btn-light border border-dark">Submit</button>
                                    <button type="button"
                                        class="btn btn-md btn-light border border-dark cancelbutton">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>