<?php namespace App\Models;

use CodeIgniter\Model;

class OrderlineModel extends Model {
protected $table ='Orderline';

protected $allowedFields =[
    'amount','product_ID','order_ID'

];

}