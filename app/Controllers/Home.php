<?php namespace App\Controllers;

use App\Models\ProductModel;

// Etusivu
class Home extends BaseController
{	

	public function __construct() {
		$session = \Config\Services::session();
		$session->start();

		$this->model = new ProductModel();
		$this->data['products'] = $this->model->getAllProducts();
		$this->data['categories'] = $this->model->getAllCategories();

		if (!isset($_SESSION['cart'])){
			$_SESSION['cart']=array(); 
		}

		// Tämä carusellia varten, aina kaikki tuotteet näkyvillä
		// Tämä carusellia varten, aina kaikki julkaistut tuotteet näkyvillä riippumatta mitä search näyttää
		$this->data['ads'] = $this->model->getAllProductsDespiteVisibility();
	}
		
	public function index() {
		
		$data = $this->data;
		$data['totalProducts'] = count($data['products']);

		echo view('templates/header', $data);
		echo view('frontpage_view', $data);
		echo view('templates/footer');
	}

	public function selectCategory($id) {
		$model = $this->model;
		$data = $this->data;
		$data['products'] = $model->getProductsByCategory($id);
		$data['selectedCategory'] = $model->getProductOrCategory($id, 'category');
		$data['totalProducts'] = count($data['products']);

		echo view('templates/header', $data);
		echo view('frontpage_view', $data);
		echo view('templates/footer');
	}

	public function search($id) {
		$model = $this->model;
		$data = $this->data;

		$criteria = $this->request->getVar('search');
		$criteria = explode(" ", $criteria);
		
		if ($id != 0) {
			$data['selectedCategory'] = $model->getProductOrCategory($id, 'category');
		}

		$data['products'] = $model->getProductsByWords($criteria, $id);

		$data['totalProducts'] = count($data['products']);

		echo view('templates/header', $data);
		echo view('frontpage_view', $data);
		echo view('templates/footer');
	}

}
